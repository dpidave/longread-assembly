#!/bin/bash 

# set path to software
PTH=/home/dpidave/software/canu-assembler/canu-1.7/Linux-amd64/bin

$PTH/canu -p ecoli -d ./ecoli-pacbio genomeSize=4.8m -pacbio-raw ./data/pacbio.fastq
