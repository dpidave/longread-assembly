# Pacbio and nonopore assembly notes using canu  

The canu guide is really good, see
http://canu.readthedocs.io/en/latest/quick-start.html for some good ideas.  

### Scripts  
Simple backbone scripts for running *canu* on pacbio and nanopore data.  

### Prerequistts 

```
#GNU plot
sudo apt-get install gnuplot

# bloody java
#
https://askubuntu.com/questions/430434/replace-openjdk-with-oracle-jdk-on-ubuntu
# I had bloody trouble with java, use these instructions

sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java7-installer

# remove open jdk
sudo apt-get purge openjdk-\*
```

## Quick start notes  
#### Low coverage genomes  
When assembling, we adjust correctedErrorRate to accomodate the slightly lower
quality corrected reads:  

```
# curl -L -o yeast.20x.fastq.gz
# http://gembox.cbcb.umd.edu/mhap/raw/yeast_filtered.20x.fastq.gz

canu \
 -p asm -d yeast \
 genomeSize=12.1m \
 correctedErrorRate=0.105 \
 -pacbio-raw yeast.20x.fastq.gz
```

#### Manually run the three steps   

```
# correct  
canu -correct \
  -p ecoli -d ecoli \
  genomeSize=4.8m \
  -pacbio-raw  pacbio.fastq
Then, trim the output of the correction:

# trim
canu -trim \
  -p ecoli -d ecoli \
  genomeSize=4.8m \
  -pacbio-corrected ecoli/ecoli.correctedReads.fasta.gz

# Assemble with different stringency of overlaps (see correctedErrorRate)  
# ie less than 30x, increase the allowed differences in the overlaps by a few
# percent (4.5% to 8.5% with correctedErrorRate=0.105 for PacBio.  
# CorMinCoverage will automatically reduced to zero to correct as many reads as
# possible.  


# assemb error rate 1
canu -assemble \
  -p ecoli -d ecoli-erate-0.039 \
  genomeSize=4.8m \
  correctedErrorRate=0.039 \
  -pacbio-corrected ecoli/ecoli.trimmedReads.fasta.gz

# assemb error rate 2
canu -assemble \
  -p ecoli -d ecoli-erate-0.075 \
  genomeSize=4.8m \
  correctedErrorRate=0.075 \
  -pacbio-corrected ecoli/ecoli.trimmedReads.fasta.gz
```

## Random notes mostly copied from the docs   

### Introduction
Canu is designed to assembly high noise long read data from PacBio and Oxford
nanopore technology. The binary release is available here:
https://github.com/marbl/canu/releases. The documents are here:
http://canu.readthedocs.io/en/latest/.  

#### The pipeline has three phases
-  Error correction  
-  Trimming  
-  Assembly  

#### infiles
The pipelines works with different reads depending on how much pre-processing
has occurred.  
```-pacbio-raw``` indicates the reads were generated on a PacBio RSII, and have had
no processing done to them. Each file of reads supplied this way becomes a
library, a specific set of parameters is applied to each library, and this is
available in a text gkp file.  

#### Workflow
1.  Load reads into the database, gkpStore  
2.  Computer k-mer counts in prep for overlap  
3.  Computer overlaps  
4.  Load overlaps into oviStore database  
5.  Run the main workflow  
6.  Read correction of data  
7.  Identify high quality sequence based on overlaps and then trim the rest  
8.  Unitig construction, finds sets of overlaps, create consensus  

Because many of the steps in the workflow use similar processes, tags are used
to define common parts, for example, corOvlMemory will set the memory usage for
the overlaps being generated during read correction: cor = read correction, Ovl
= overlapper, Memory  = memory.  

#### Error rate reporting
Error rates are reported as fractions, so 0.12 is 12%. Note that the Canu error
rates always refer to the percent difference in an alignment of two reads, not
the percent error in a single read, and not the amount of variation in your
reads.  

There are seven error rates, three used during overlap creation
(corOvlErrorRate, optOvlErrorRate, and utgOvlErrorRate. There are four that
control the main alg (corErrorRate, obtErrorRate, utgErrorRate, and
utgOvlErrorRate.  

*Mhap* is used to generate the corrected overlaps.

#### Minimum lengths
```minReadLength``` discard reads shorter than this when loaded into the assembler,
and when trimming reads

```minOverlapLength``` do not save overlaps shorter than this.

#### Overlap config
For each overlapper, ‘ovl’ or ‘mhap’, there is a global config and three spec
ones that apply to each stage. The tags in this case are ‘cor’, ‘obt’, and
‘utg’.  

For example, to change k-mer for all instances of the ovl overlapper, ‘merSize=23’
would be used. To change k-mer size for just the correction, corMerSize=16, etc.
full list of paramater settings is available here
http://canu.readthedocs.io/en/latest/parameter-reference.html#parameter-reference

#### Outputs  
Most outputs go to stdout, but you can also see them in <prefix>.report.

BTW unitig is a contig seed, which is high confidence sequence that breaks at
ambiguity.

#### Logging  
<prefix>.report

Reads  
<prefix>.correctedReads.fasta.gz - reads after correction  
<prefix>.trimmedReads.fasta.gz - reads after trimming  

#### Sequences
<prefix>.contigs.fasta - everything that was assembled in the primary assembly,
including both unique a repetitive sequences  
<prefix>.unitigs.fasta - contigs, split at alternative points in the graph.  
<prefix>.unassembled.fasta - reads and low coverage contigs that were too low
coverage to assemble.  

