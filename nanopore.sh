#!/bin/bash

# see useful help: http://canu.readthedocs.io/en/latest/quick-start.html
# set path to software
PTH=/home/dpidave/software/canu-assembler/canu-1.7/Linux-amd64/bin

$PTH/canu -p ecoli -d ./ecoli-nanopore genomeSize=4.8m \
  -nanopore-raw ./nanopore/oxford.fasta
